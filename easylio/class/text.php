<?php

class Text{

	public function retrievePaths($md, $id){

		$basePaths = ['src="', 'poster="', "background-image:url('"];

		foreach($basePaths as $basePath){

			if(strpos($md, $basePath) > -1){

				$path = ($folder !== false) ? $basePath.'content/'.$id.'/' : $basePath.'content/';

				$md = str_replace($basePath, $path, $md);

			}else{

				$md = $md;
			}

		}
		return $md;

	}


	public function getPart($text, $name){

		$splitFirst = "<--- ";
		$splitLast = " --->";
		$needle = $splitFirst.$name.$splitLast;

		$lenLast = strlen($splitFirst);
		$lenFirst = strlen($needle);

		$pos1 = strpos($text, $needle)+$lenFirst;

		if($pos1 !== false){

			$sub1 = substr($text, $pos1);
			$pos2 = (strpos($sub1, $splitFirst) !== -1) ? strpos($sub1, $splitFirst) : false;

			$sub = ($pos2) ? substr($text, $pos1, $pos2) : substr($text, $pos1);

			return $sub;

		}else{

			return '';
		}

	}

	public function parse($filename, $folder = false, $name = false){

		$path = ($folder !== false) ? $folder."/".$filename.".md" : "content/".$filename.".md";
		$file = file_get_contents($path);
		$parsedown = new Parsedown();

		if($name == false){

			return $this->retrievePaths($parsedown->text($file), basename($folder));

		}else{

			$content = $this->getPart($file, $name);

			return ($name == 'medias') ? $this->retrievePaths($parsedown->text($content), basename($folder)) : $parsedown->text($content);


		}
	}

	public function karaoParse($filename, $folder = false){

		return '<div class="kara">'.$this->parse($filename, $folder).'</div>';
	}

}