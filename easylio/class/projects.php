<?php

class Projects extends Text{


	public function hasProjectFile($dir){

		$files = glob($dir."/*");
		$projectFile = $dir."/project.md";

		return (in_array($projectFile, $files)) ? true : false;
	}

	public function setProject($dir){

		$parts = ['header', 'medias', 'about'];
		$project = new stdClass();

		foreach($parts as $part){

			$project->$part = $this->parse('project', $dir, $part);

		}


		$project->id = substr(basename($dir), strpos(basename($dir),"-")+1);
		$project->path = basename($dir);

		return $project;

	}

	public function all(){

		$dirs = array_filter(glob('content/*'), 'is_dir');
		$projects = [];
		

		foreach($dirs as $dir){

			if($this->hasProjectFile($dir)){

				$projects [] = $this->setProject($dir);

			}

		}

		return $projects;

	}
	

	public function id($id){

		$id = preg_replace('[^a-zA-Z0-9]', '', $id);
		$dir = '../../content/'.$id;
		$dirs = array_filter(glob('../../content/*'), 'is_dir');

		if(in_array($dir, $dirs)){

			return $this->setProject($dir);

		}
		
	}
}