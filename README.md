# Easylio

A simple PHP structure to make an online portfolio. Based on markdown files.
Uses the library [Parsedown](https://parsedown.org/).

Place your images inside the folder of the project (no image in demo version)