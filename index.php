<?php require("easylio/launch.php") ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Easylio, the easy easy portfolio ♡</title>
	<link rel="stylesheet" type="text/css" href="assets/css/main.css">
</head>
<body>
	<?php include("site/snippets/header.php") ?>
	<main>
		<?php foreach($projects->all() as $project): ?>
			<section id="<?= $project->id ?>" data-path="<?= $project->path ?>" class="project">
				<header>
					<?= $project->header ?>
				</header>
				<div class="content">
					
				</div>
			</section>
		<?php endforeach ?>	
	</main>
	<?php include("site/snippets/footer.php") ?>
</body>
</html>