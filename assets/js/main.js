
function mediaLoaded(container, part){

	var firstElement = container.children[0].children[0].children[0];

	if(firstElement.tagName == 'VIDEO'){

		firstElement.addEventListener("loadeddata", function(){

			part.classList.add("on");

		});

		setTimeout(() => {

			part.classList.add("on");

		}, 1000);

	}else if(firstElement.children[0].tagName == 'IMG'){

		firstElement.children[0].addEventListener("load", function(){

			part.classList.add("on");

		});

	}else{

		part.classList.add("on");
	}
}

function figCapting(part){

	var imgs = part.querySelectorAll("img");

	for(k=0; k<imgs.length; k++){

		var parent = imgs[k].parentElement;
		var keepIm = imgs[k];
		var figure = document.createElement("figure");
		var figCap = document.createElement("figcaption");

		imgs[k].outerHTML="";
		figCap.innerText=keepIm.alt;

		figure.appendChild(keepIm);
		figure.appendChild(figCap);
		parent.appendChild(figure);


	}
}


function loadContent(id, container, part) {

	var xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function(){

		if (this.readyState == 4 && this.status == 200) {

			container.innerHTML = this.responseText;
			part.classList.add("loaded");

			figCapting(part);
			mediaLoaded(container, part);


		}

	};

	xhttp.open("GET", "site/snippets/content.php?id="+id, true);
	xhttp.send();

}



function stopVideos(part){

	var videos = part.getElementsByTagName("VIDEO");

	 for(j=0; j<videos.length; j++){

		videos[j].pause();
		videos[j].currentTime=0;

	}

}


function playVideos(part){

	var videos = part.getElementsByTagName("VIDEO");

	 for(j=0; j<videos.length; j++){

		videos[j].play();

	}

}


function openClose(){

	var parts = document.querySelectorAll(".project");

	for(i=0; i<parts.length; i++){

		var header = parts[i].getElementsByTagName('header')[0];

		header.addEventListener("click", function() {

			var parent = this.parentElement;

			if(parent.classList.contains("on")){

				parent.classList.remove("on");
				stopVideos(parent);

			}else{

				if(parent.classList.contains("loaded")){

					playVideos(parent);
					parent.classList.add("on");

				}else{

					var id = parent.dataset.path; 
					var container = parent.querySelector(".content"); 

					loadContent(id, container, parent);
				}
			}

		});

	}
}

openClose();
